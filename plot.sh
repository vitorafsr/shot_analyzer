#!/bin/bash

if [[ -z $1 ]]; then
    echo "give filename as argument"
    exit 0;
fi

echo 'scanning'
size=600
fs1=$(stat --printf="%s" $1)
while true; do
    echo -n '.'
    fs2=$(stat --printf="%s" $1)
    if [[ $(($fs2 - $fs1)) -eq $size ]]; then
        sf=$(($fs2-$size))
        gnuplot -p -e 'set title "Magnetometer data"' -e "set yrange [0:255]" -e "plot [0:200] \"$1\" binary"' format="%uint8%uint8%uint8" skip='"$sf"' using 1 with lines title "X data" smooth csplines' -e "replot \"$1\" binary"' format="%uint8%uint8%uint8" skip='"$sf"' using 2 with lines title "Y data" smooth csplines' -e "replot \"$1\" binary"' format="%uint8%uint8%uint8" skip='"$sf"' using 3 with lines title "Z data" smooth csplines'
        fs1=$fs2
    fi
    sleep 1
done
