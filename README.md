# Shot Analyzer #

This project was developed for a [RoadTest review of the B-L475E-IOT01A Discovery kit for IoT node](https://www.element14.com/community/roadTests/1803/l/stmicroelectronics-discovery-kit-for-iot-node).

To that, I designed a shot analyzer using the board magnetometer. The ideia is to attach the board to a gun during shoting to record gun position prior to firing. Then, after firing, the user press the board user button to submit data for ploting in a PC.

The plot is a aid to the shot instructor to analyze the common mistakes people do while firing that are not possible to be detected by the hole in the target because the error is so large that the bullet does not touch the target.

The review when finished will be publish at the [Element14 Community site](https://www.element14.com/community/welcome).
